<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Cache;

use Exception;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\CacheItemRepository;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\RemoteConnection;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Serializer;
use RuntimeException;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class Warmup implements CacheWarmerInterface
{
    /** @var RemoteConnection */
    private $connection;

    /** @var Serializer */
    private $serializer;

    /** @var CacheItemRepository */
    private $cacheItemRepository;

    /** @var string[] */
    private $locales;

    /**
     * @var string
     */
    private $env;

    /**
     * @param KernelInterface $kernel
     * @param RemoteConnection $connection
     * @param Serializer $serializer
     * @param CacheItemRepository $cacheItemRepository
     * @param string[] $locales
     */
    public function __construct(
        KernelInterface $kernel,
        RemoteConnection $connection,
        Serializer $serializer,
        CacheItemRepository $cacheItemRepository,
        array $locales
    ) {
        $this->env = $kernel->getEnvironment();
        $this->connection = $connection;
        $this->serializer = $serializer;
        $this->cacheItemRepository = $cacheItemRepository;
        $this->locales = $locales;
    }

    public function isOptional(): bool
    {
        return false;
    }

    public function warmUp($cacheDir): void
    {
        if (isset($_SERVER['REMOTE_ADDR']) || $this->env === 'test') {
            return;
        }

        $languages = $this->locales;
        foreach ($languages as $language) {
            $cacheItem = $this->cacheItemRepository->getItem($language);
            $header = null;
            $loop = 0;
            while ($header === null && $loop < 10) {
                $loop++;
                try {
                    $headerJson = $this->connection->downloadMenus($language);
                    $header = $this->serializer->deserialize($headerJson);
                } catch (Exception $e) {
                    sleep(1);
                }
            }

            if ($header === null) {
                throw new RuntimeException(
                    'Cache warmup failed for Header and Footer. (Check your internet connection)'
                );
            }

            $cacheItem->set($header);
            $this->cacheItemRepository->storeItem($cacheItem);
        }
    }
}
