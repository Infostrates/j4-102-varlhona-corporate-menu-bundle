<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu;

use Infostrates\ValrhonaCorporateMenu\DependencyInjection\CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class InfostratesValrhonaCorporateMenuBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new CompilerPass());
    }
}
