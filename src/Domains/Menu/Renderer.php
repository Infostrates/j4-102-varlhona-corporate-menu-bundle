<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\MenuComponentInterface;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\WebAssets;
use InvalidArgumentException;
use Symfony\Bridge\Twig\AppVariable;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Renderer
{
    /** @var Environment */
    private $templating;

    public function __construct(Environment $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param MenuComponentInterface $menuComponent
     * @param array<mixed> $options
     * @param string|null $variation
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(
        MenuComponentInterface $menuComponent,
        array $options = [],
        ?string $variation = null
    ): string {
        $componentTypeIdentifier = $this->getComponentTypeIdentifier($menuComponent);

        $templateIdentifier = $componentTypeIdentifier;
        if ($variation) {
            $templateIdentifier .= '-' . $variation;
        }

        return $this->templating->render(
            '@InfostratesValrhonaCorporateMenu/' . $templateIdentifier . '.html.twig',
            [
                $componentTypeIdentifier => $menuComponent,
                'options' => $options,
            ]
        );
    }

    private function getComponentTypeIdentifier(MenuComponentInterface $menuComponent): string
    {
        switch (get_class($menuComponent)) {
            case Header::class:
                return 'header';
            case WebAssets::class:
                return 'web_assets';
            case Footer::class:
                return 'footer';
            default:
                throw new InvalidArgumentException('Unhandled menu component');
        }
    }
}
