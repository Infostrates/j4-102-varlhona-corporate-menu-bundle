<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common;

class Button
{
    /** @var Image */
    private $icon;

    /**  @var string  */
    private $tooltip;

    /** @var Link */
    private $link;

    public function __construct(Image $icon, string $tooltip, Link $link)
    {
        $this->icon = $icon;
        $this->tooltip = $tooltip;
        $this->link = $link;
    }

    public function getIcon(): Image
    {
        return $this->icon;
    }

    public function getTooltip(): string
    {
        return $this->tooltip;
    }

    public function getLink(): Link
    {
        return $this->link;
    }
}
