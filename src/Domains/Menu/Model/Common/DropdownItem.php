<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common;

class DropdownItem
{
    /** @var string */
    private $title;

    /** @var string */
    private $url;

    /** @var string */
    private $target;

    /**
     * @param string $title
     * @param string $url
     * @param string $target
     */
    public function __construct(string $title, string $url, string $target = '_self')
    {
        $this->title = $title;
        $this->url = $url;
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }
}
