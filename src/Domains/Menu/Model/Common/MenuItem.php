<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\Push;

class MenuItem
{
    /** @var string */
    private $label;

    /** @var Link|null */
    private $link;

    /** @var string|null */
    private $type;

    /** @var MenuItem[] */
    private $children;

    /** @var \Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\Push|null */
    private $push;

    /** @var string|null */
    private $iconUrl;

    /**
     * @param string      $label
     * @param Link|null   $link
     * @param string|null $type
     * @param MenuItem[]  $children
     * @param Push|null   $push
     * @param string|null $iconUrl
     */
    public function __construct(
        string $label,
        ?Link $link,
        ?string $type = null,
        array $children = [],
        ?Push $push = null,
        ?string $iconUrl = null
    ) {
        $this->label = $label;
        $this->link = $link;
        $this->type = $type;
        $this->children = $children;
        $this->push = $push;
        $this->iconUrl = $iconUrl;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    public function getPush(): ?Push
    {
        return $this->push;
    }

    public function getIconUrl(): ?string
    {
        return $this->iconUrl;
    }
}
