<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common;

class MainLogo
{
    /** @var Image */
    private $image;

    /** @var string */
    private $url;

    public function __construct(Image $image, string $url)
    {
        $this->image = $image;
        $this->url = $url;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
