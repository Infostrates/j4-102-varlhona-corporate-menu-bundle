<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common;

class Image
{
    /** @var string */
    private $url;

    /** @var string|null */
    private $alt;

    public function __construct(string $url, ?string $alt = null)
    {
        $this->url = $url;
        $this->alt = $alt;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }
}
