<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common;

class Link
{
    public const TARGET_BLANK = '_blank';

    /** @var string */
    private $url;

    /** @var string|null */
    private $target;

    /**
     * @param string      $url
     * @param string|null $target
     */
    public function __construct(string $url, ?string $target = null)
    {
        $this->url = $url;
        $this->target = $target;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }
}
