<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Button;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\MainLogo;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\MenuItem;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\AdditionalAssets;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\AdditionalWordings;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\EventBanner;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\LayoutHeaderMenu;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\Search;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\UserMenu;

class Header implements MenuComponentInterface
{
    /** @var EventBanner[] */
    private $eventBannerList;

    /** @var MainLogo */
    private $mainLogo;

    /** @var Search|null */
    private $search;

    /** @var Button[] */
    private $buttonList;

    /** @var MenuItem[] */
    private $mainMenu;

    /** @var AdditionalAssets */
    private $additionalAssets;

    /** @var AdditionalWordings */
    private $additionalWordings;

    /** @var UserMenu */
    private $userMenu;

    /** @var LayoutHeaderMenu|null */
    private $layoutHeaderMenu;

    /**
     * @param EventBanner[] $eventBannerList
     * @param MainLogo $mainLogo
     * @param Search|null $search
     * @param Button[] $buttonList
     * @param MenuItem[] $mainMenu
     * @param AdditionalAssets $additionalAssets
     * @param AdditionalWordings $additionalWordings
     * @param UserMenu $userMenu
     * @param LayoutHeaderMenu|null $layoutHeaderMenu
     */
    public function __construct(
        array $eventBannerList,
        MainLogo $mainLogo,
        ?Search $search,
        array $buttonList,
        array $mainMenu,
        AdditionalAssets $additionalAssets,
        AdditionalWordings $additionalWordings,
        UserMenu $userMenu,
        ?LayoutHeaderMenu $layoutHeaderMenu
    ) {
        $this->eventBannerList = $eventBannerList;
        $this->mainLogo = $mainLogo;
        $this->search = $search;
        $this->buttonList = $buttonList;
        $this->mainMenu = $mainMenu;
        $this->additionalAssets = $additionalAssets;
        $this->additionalWordings = $additionalWordings;
        $this->userMenu = $userMenu;
        $this->layoutHeaderMenu = $layoutHeaderMenu;
    }

    /**
     * @return EventBanner[]
     */
    public function getEventBannerList(): array
    {
        return $this->eventBannerList;
    }

    public function getMainLogo(): MainLogo
    {
        return $this->mainLogo;
    }

    public function getSearch(): ?Search
    {
        return $this->search;
    }

    /**
     * @return Button[]
     */
    public function getButtonList(): array
    {
        return $this->buttonList;
    }

    /**
     * @return MenuItem[]
     */
    public function getMainMenu(): array
    {
        return $this->mainMenu;
    }

    public function getAdditionalAssets(): AdditionalAssets
    {
        return $this->additionalAssets;
    }

    public function getAdditionalWordings(): AdditionalWordings
    {
        return $this->additionalWordings;
    }

    /**
     * @return UserMenu
     */
    public function getUserMenu(): UserMenu
    {
        return $this->userMenu;
    }

    /**
     * @return LayoutHeaderMenu|null
     */
    public function getLayoutHeaderMenu(): ?LayoutHeaderMenu
    {
        return $this->layoutHeaderMenu;
    }
}
