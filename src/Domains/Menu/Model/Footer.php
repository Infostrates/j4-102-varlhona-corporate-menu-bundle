<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Image;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\AdditionalAssets;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\AdditionalWordings;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\BCorpIcon;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\ButtonsContainer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\CountryAccess;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\LanguageAccess;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\MenuItemsContainer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer\Zone;

class Footer implements MenuComponentInterface
{
    /** @var Image */
    private $mainLogo;

    /** @var Zone[] */
    private $zoneList;

    /** @var CountryAccess */
    private $currentCountryAccess;

    /** @var LanguageAccess[] */
    private $languageAccessList;

    /** @var LanguageAccess */
    private $currentLanguageAccess;

    /** @var MenuItemsContainer|null */
    private $menuItemContainer1;

    /** @var MenuItemsContainer|null */
    private $menuItemContainer2;

    /** @var ButtonsContainer|null */
    private $buttonsContainer;

    /** @var BCorpIcon */
    private $bCorpIcon;

    /** @var MenuItemsContainer|null */
    private $menuItemContainer3;

    /** @var AdditionalAssets */
    private $additionalAssets;

    /** @var AdditionalWordings */
    private $additionalWordings;

    /**
     * @param Image                   $mainLogo
     * @param Zone[]                  $zoneList
     * @param CountryAccess           $currentCountryAccess
     * @param LanguageAccess[]        $languageAccessList
     * @param LanguageAccess          $currentLanguageAccess
     * @param MenuItemsContainer|null $menuItemContainer1
     * @param MenuItemsContainer|null $menuItemContainer2
     * @param ButtonsContainer|null   $buttonsContainer
     * @param BCorpIcon               $bCorpIcon
     * @param MenuItemsContainer|null $menuItemContainer3
     * @param AdditionalAssets        $additionalAssets
     * @param AdditionalWordings      $additionalWordings
     */
    public function __construct(
        Image $mainLogo,
        array $zoneList,
        CountryAccess $currentCountryAccess,
        array $languageAccessList,
        LanguageAccess $currentLanguageAccess,
        ?MenuItemsContainer $menuItemContainer1,
        ?MenuItemsContainer $menuItemContainer2,
        ?ButtonsContainer $buttonsContainer,
        BCorpIcon $bCorpIcon,
        ?MenuItemsContainer $menuItemContainer3,
        AdditionalAssets $additionalAssets,
        AdditionalWordings $additionalWordings
    ) {
        $this->mainLogo = $mainLogo;
        $this->zoneList = $zoneList;
        $this->currentCountryAccess = $currentCountryAccess;
        $this->languageAccessList = $languageAccessList;
        $this->currentLanguageAccess = $currentLanguageAccess;
        $this->menuItemContainer1 = $menuItemContainer1;
        $this->menuItemContainer2 = $menuItemContainer2;
        $this->buttonsContainer = $buttonsContainer;
        $this->bCorpIcon = $bCorpIcon;
        $this->menuItemContainer3 = $menuItemContainer3;
        $this->additionalAssets = $additionalAssets;
        $this->additionalWordings = $additionalWordings;
    }

    public function getMainLogo(): Image
    {
        return $this->mainLogo;
    }

    /**
     * @return Zone[]
     */
    public function getZoneList(): array
    {
        return $this->zoneList;
    }

    public function getCurrentCountryAccess(): CountryAccess
    {
        return $this->currentCountryAccess;
    }

    /**
     * @return LanguageAccess[]
     */
    public function getLanguageAccessList(): array
    {
        return $this->languageAccessList;
    }

    public function getCurrentLanguageAccess(): LanguageAccess
    {
        return $this->currentLanguageAccess;
    }

    public function getMenuItemContainer1(): ?MenuItemsContainer
    {
        return $this->menuItemContainer1;
    }

    public function getMenuItemContainer2(): ?MenuItemsContainer
    {
        return $this->menuItemContainer2;
    }

    public function getButtonsContainer(): ?ButtonsContainer
    {
        return $this->buttonsContainer;
    }

    public function getBCorpIcon(): BCorpIcon
    {
        return $this->bCorpIcon;
    }

    public function getMenuItemContainer3(): ?MenuItemsContainer
    {
        return $this->menuItemContainer3;
    }

    public function getAdditionalAssets(): AdditionalAssets
    {
        return $this->additionalAssets;
    }

    public function getAdditionalWordings(): AdditionalWordings
    {
        return $this->additionalWordings;
    }
}
