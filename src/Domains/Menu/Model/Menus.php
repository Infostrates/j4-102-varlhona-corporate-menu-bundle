<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model;

class Menus
{
    /** @var WebAssets */
    private $webAssets;

    /** @var Header */
    private $header;

    /** @var Footer */
    private $footer;

    /**
     * @param WebAssets $webAssets
     * @param Header    $header
     * @param Footer    $footer
     */
    public function __construct(WebAssets $webAssets, Header $header, Footer $footer)
    {
        $this->webAssets = $webAssets;
        $this->header = $header;
        $this->footer = $footer;
    }

    public function getWebAssets(): WebAssets
    {
        return $this->webAssets;
    }

    public function getHeader(): Header
    {
        return $this->header;
    }

    public function getFooter(): Footer
    {
        return $this->footer;
    }
}
