<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\DropdownItem;

class UserMenu
{
    /**
     * @var string
     */
    private $loginLabel;

    /**
     * @var string
     */
    private $loginUrl;

    /**
     * @var string
     */
    private $loginIcon;

    /**
     * @var string
     */
    private $logoutLabel;

    /**
     * @var string
     */
    private $logoutUrl;

    /**
     * @var string
     */
    private $logoutIcon;

    /**
     * @var DropdownItem[]
     */
    private $items;

    /**
     * @param string $loginLabel
     * @param string $loginUrl
     * @param string $loginIcon
     * @param string $logoutLabel
     * @param string $logoutUrl
     * @param string $logoutIcon
     * @param DropdownItem[] $items
     */
    public function __construct(
        string $loginLabel,
        string $loginUrl,
        string $loginIcon,
        string $logoutLabel,
        string $logoutUrl,
        string $logoutIcon,
        array $items
    ) {
        $this->loginLabel = $loginLabel;
        $this->loginUrl = $loginUrl;
        $this->loginIcon = $loginIcon;
        $this->logoutLabel = $logoutLabel;
        $this->logoutUrl = $logoutUrl;
        $this->logoutIcon = $logoutIcon;
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getLoginLabel(): string
    {
        return $this->loginLabel;
    }

    /**
     * @return string
     */
    public function getLoginUrl(): string
    {
        return $this->loginUrl;
    }

    /**
     * @return string
     */
    public function getLoginIcon(): string
    {
        return $this->loginIcon;
    }

    /**
     * @return string
     */
    public function getLogoutLabel(): string
    {
        return $this->logoutLabel;
    }

    /**
     * @return string
     */
    public function getLogoutUrl(): string
    {
        return $this->logoutUrl;
    }

    /**
     * @return string
     */
    public function getLogoutIcon(): string
    {
        return $this->logoutIcon;
    }

    /**
     * @return DropdownItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
