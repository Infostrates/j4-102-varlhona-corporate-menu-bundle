<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;

class LayoutHeaderMenu
{
    /** @var string */
    private $link;

    /** @var string */
    private $iconBlack;

    /** @var string */
    private $iconWhite;

    /**
     * @param string $link
     * @param string $iconBlack
     * @param string $iconWhite
     */
    public function __construct(
        string $link,
        string $iconBlack,
        string $iconWhite
    ) {
        $this->link = $link;
        $this->iconBlack = $iconBlack;
        $this->iconWhite = $iconWhite;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getIconBlack(): string
    {
        return $this->iconBlack;
    }

    /**
     * @return string
     */
    public function getIconWhite(): string
    {
        return $this->iconWhite;
    }
}
