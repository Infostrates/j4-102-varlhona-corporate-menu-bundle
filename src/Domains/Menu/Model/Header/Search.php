<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Image;

class Search
{
    /** @var Image */
    private $icon;

    /** @var string */
    private $tooltip;

    /** @var string */
    private $actionUrl;

    /** @var string */
    private $paramName;

    /** @var string */
    private $inputPlaceholder;

    /**
     * @param Image  $icon
     * @param string $tooltip
     * @param string $actionUrl
     * @param string $paramName
     * @param string $inputPlaceholder
     */
    public function __construct(
        Image $icon,
        string $tooltip,
        string $actionUrl,
        string $paramName,
        string $inputPlaceholder
    ) {
        $this->icon = $icon;
        $this->tooltip = $tooltip;
        $this->actionUrl = $actionUrl;
        $this->paramName = $paramName;
        $this->inputPlaceholder = $inputPlaceholder;
    }

    public function getIcon(): Image
    {
        return $this->icon;
    }

    public function getTooltip(): string
    {
        return $this->tooltip;
    }

    public function getActionUrl(): string
    {
        return $this->actionUrl;
    }

    public function getParamName(): string
    {
        return $this->paramName;
    }

    public function getInputPlaceholder(): string
    {
        return $this->inputPlaceholder;
    }
}
