<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;

class AdditionalAssets
{
    /** @var string */
    private $close;

    /** @var string */
    private $arrowRight;

    /** @var string */
    private $burger;

    /** @var string */
    private $closeModal;

    public function __construct(string $close, string $arrowRight, string $burger, string $closeModal)
    {
        $this->close = $close;
        $this->arrowRight = $arrowRight;
        $this->burger = $burger;
        $this->closeModal = $closeModal;
    }

    public function getClose(): string
    {
        return $this->close;
    }

    public function getArrowRight(): string
    {
        return $this->arrowRight;
    }

    public function getBurger(): string
    {
        return $this->burger;
    }

    public function getCloseModal(): string
    {
        return $this->closeModal;
    }
}
