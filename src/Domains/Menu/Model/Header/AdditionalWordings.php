<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;

class AdditionalWordings
{
    /** @var string */
    private $generalEmbedCursorSee;

    public function __construct(string $generalEmbedCursorSee)
    {
        $this->generalEmbedCursorSee = $generalEmbedCursorSee;
    }

    /**
     * @return string
     */
    public function getGeneralEmbedCursorSee(): string
    {
        return $this->generalEmbedCursorSee;
    }
}
