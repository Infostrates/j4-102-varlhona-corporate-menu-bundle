<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Image;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Link;

class Push
{
    /** @var Image */
    private $image;

    /** @var string */
    private $label;

    /** @var Link|null */
    private $link;

    /**
     * @param Image     $image
     * @param string    $label
     * @param Link|null $link
     */
    public function __construct(Image $image, string $label, ?Link $link)
    {
        $this->image = $image;
        $this->label = $label;
        $this->link = $link;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }
}
