<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model;

class WebAssets implements MenuComponentInterface
{
    /** @var string[] */
    private $cssUrlList;

    /** @var string[] */
    private $jsUrlList;

    /**
     * @param string[] $cssUrlList
     * @param string[] $jsUrlList
     */
    public function __construct(array $cssUrlList, array $jsUrlList)
    {
        $this->cssUrlList = $cssUrlList;
        $this->jsUrlList = $jsUrlList;
    }

    /**
     * @return string[]
     */
    public function getCssUrlList(): array
    {
        return $this->cssUrlList;
    }

    /**
     * @return string[]
     */
    public function getJsUrlList(): array
    {
        return $this->jsUrlList;
    }
}
