<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

class LanguageAccess
{
    /** @var string */
    private $label;

    /** @var string */
    private $languageIdentifier;

    /** @var string */
    private $siteAccessName;

    /** @var string */
    private $url;

    /**
     * @param string $label
     * @param string $languageIdentifier
     * @param string $siteAccessName
     * @param string $url
     */
    public function __construct(string $label, string $languageIdentifier, string $siteAccessName, string $url)
    {
        $this->label = $label;
        $this->languageIdentifier = $languageIdentifier;
        $this->siteAccessName = $siteAccessName;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getLanguageIdentifier(): string
    {
        return $this->languageIdentifier;
    }

    /**
     * @return string
     */
    public function getSiteAccessName(): string
    {
        return $this->siteAccessName;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
