<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

class AdditionalWordings
{
    /** @var string */
    private $footerLogoIntroMessage;

    /** @var string */
    private $languageSelectorLocationDropdownIntroMessage;

    /** @var string */
    private $countrySelectorCountryDropDownIntroMessage;

    /** @var string */
    private $languageSelectorLanguageDropdownIntroMessage;

    public function __construct(
        string $footerLogoIntroMessage,
        string $languageSelectorLocationDropdownIntroMessage,
        string $countrySelectorCountryDropDownIntroMessage,
        string $languageSelectorLanguageDropdownIntroMessage
    ) {
        $this->footerLogoIntroMessage = $footerLogoIntroMessage;
        $this->languageSelectorLocationDropdownIntroMessage = $languageSelectorLocationDropdownIntroMessage;
        $this->countrySelectorCountryDropDownIntroMessage = $countrySelectorCountryDropDownIntroMessage;
        $this->languageSelectorLanguageDropdownIntroMessage = $languageSelectorLanguageDropdownIntroMessage;
    }

    public function getFooterLogoIntroMessage(): string
    {
        return $this->footerLogoIntroMessage;
    }

    public function getLanguageSelectorLocationDropdownIntroMessage(): string
    {
        return $this->languageSelectorLocationDropdownIntroMessage;
    }

    public function getCountrySelectorCountryDropDownIntroMessage(): string
    {
        return $this->countrySelectorCountryDropDownIntroMessage;
    }

    public function getLanguageSelectorLanguageDropdownIntroMessage(): string
    {
        return $this->languageSelectorLanguageDropdownIntroMessage;
    }
}
