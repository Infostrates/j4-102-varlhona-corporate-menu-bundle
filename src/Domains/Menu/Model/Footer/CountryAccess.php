<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

use DomainException;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CountryAccess implements NormalizableInterface
{
    /** @var string */
    private $label;

    /** @var string */
    private $countryIdentifier;

    /** @var LanguageAccess[] */
    private $languageAccessList;

    /**
     * @param string           $label
     * @param string           $countryIdentifier
     * @param LanguageAccess[] $languageAccessList
     */
    public function __construct(string $label, string $countryIdentifier, array $languageAccessList)
    {
        $this->label = $label;
        $this->countryIdentifier = $countryIdentifier;
        $this->languageAccessList = $languageAccessList;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getCountryIdentifier(): string
    {
        return $this->countryIdentifier;
    }

    /**
     * @return LanguageAccess[]
     */
    public function getLanguageAccessList(): array
    {
        return $this->languageAccessList;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        if (!isset($this->getLanguageAccessList()[0])) {
            throw new DomainException('No language access for this country. Set it in ezplatform.yml');
        }

        return $this->getLanguageAccessList()[0]->getUrl();
    }

    /**
     * @param NormalizerInterface  $normalizer
     * @param string|null          $format
     * @param array<string, mixed> $context
     * @return array<string, mixed>
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = []): array
    {
        return [
            'label' => $this->label,
            'countryIdentifier' => $this->countryIdentifier,
            'languageAccessList' => $normalizer->normalize($this->languageAccessList, $format, $context),
        ];
    }
}
