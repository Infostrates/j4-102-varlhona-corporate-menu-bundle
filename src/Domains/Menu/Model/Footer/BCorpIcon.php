<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Image;

class BCorpIcon
{
    /** @var Image */
    private $icon;

    /** @var string|null */
    private $url;

    /**
     * @param Image  $icon
     * @param string|null $url
     */
    public function __construct(Image $icon, ?string $url)
    {
        $this->icon = $icon;
        $this->url = $url;
    }

    public function getIcon(): Image
    {
        return $this->icon;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }
}
