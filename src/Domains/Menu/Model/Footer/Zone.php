<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

class Zone
{
    /** @var string */
    private $label;

    /** @var string */
    private $zoneIdentifier;

    /** @var CountryAccess[] */
    private $countryAccessList;

    /**
     * @param string          $label
     * @param string          $zoneIdentifier
     * @param CountryAccess[] $countryAccessList
     */
    public function __construct(string $label, string $zoneIdentifier, array $countryAccessList)
    {
        $this->label = $label;
        $this->zoneIdentifier = $zoneIdentifier;
        $this->countryAccessList = $countryAccessList;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getZoneIdentifier(): string
    {
        return $this->zoneIdentifier;
    }

    /**
     * @return CountryAccess[]
     */
    public function getCountryAccessList(): array
    {
        return $this->countryAccessList;
    }
}
