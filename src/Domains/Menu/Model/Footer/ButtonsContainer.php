<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Button;

class ButtonsContainer
{
    /** @var string|null */
    private $title;

    /** @var Button[] */
    private $buttonList;

    /**
     * @param string|null $title
     * @param Button[]  $buttonList
     */
    public function __construct(?string $title, array $buttonList)
    {
        $this->title = $title;
        $this->buttonList = $buttonList;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return Button[]
     */
    public function getButtonList(): array
    {
        return $this->buttonList;
    }
}
