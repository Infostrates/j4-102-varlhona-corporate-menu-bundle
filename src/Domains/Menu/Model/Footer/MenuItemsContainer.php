<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\MenuItem;

class MenuItemsContainer
{
    /** @var string|null */
    private $title;

    /** @var MenuItem[] */
    private $menuItemList;

    /**
     * @param string|null $title
     * @param MenuItem[]  $menuItemList
     */
    public function __construct(?string $title, array $menuItemList)
    {
        $this->title = $title;
        $this->menuItemList = $menuItemList;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return MenuItem[]
     */
    public function getMenuItemList(): array
    {
        return $this->menuItemList;
    }
}
