<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;

class AdditionalAssets
{
    /** @var string */
    private $poi;

    /** @var string */
    private $closeModal;

    /**
     * @param string $poi
     * @param string $closeModal
     */
    public function __construct(string $poi, string $closeModal)
    {
        $this->poi = $poi;
        $this->closeModal = $closeModal;
    }

    public function getPoi(): string
    {
        return $this->poi;
    }

    public function getCloseModal(): string
    {
        return $this->closeModal;
    }
}
