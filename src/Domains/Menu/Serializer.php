<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use DomainException;
use Exception;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Menus;
use RuntimeException;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\CustomNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as BaseSerializer;
use Symfony\Component\Serializer\SerializerInterface;

class Serializer
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct()
    {
        $propertyTypeExtractor = new PhpDocExtractor();
        $this->serializer = new BaseSerializer(
            [
                new ObjectNormalizer(),
                new CustomNormalizer(),
                new GetSetMethodNormalizer(null, null, $propertyTypeExtractor),
                new ArrayDenormalizer()
            ],
            [new JsonEncoder()]
        );
    }
    public function serialize(Menus $data): string
    {
        return $this->serializer->serialize($data, 'json');
    }

    public function deserialize(string $json): Menus
    {
        try {
            $menus = $this->serializer->deserialize($json, Menus::class, 'json');
            if (!$menus instanceof Menus) {
                throw new DomainException('Unexpected menus type');
            }

            return $menus;
        } catch (Exception $e) {
            throw new RuntimeException('Unable to deserialize json to Menus', 0, $e);
        }
    }
}
