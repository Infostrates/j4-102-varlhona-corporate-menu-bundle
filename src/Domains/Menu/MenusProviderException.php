<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use Exception;

class MenusProviderException extends Exception
{
}
