<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Menus;
use Psr\Log\LoggerInterface;
use RuntimeException;

class StaleCacheUpdater
{
    /** @var RemoteConnection */
    private $connection;

    /** @var Serializer */
    private $serializer;

    /** @var CacheItemRepository */
    private $cacheItemRepository;

    /** @var LoggerInterface|null */
    private $logger;

    /** @var bool */
    private $enabled;

    public function __construct(
        RemoteConnection $connection,
        Serializer $serializer,
        CacheItemRepository $cacheItemRepository,
        ?LoggerInterface $logger,
        bool $enabled
    ) {
        $this->connection = $connection;
        $this->serializer = $serializer;
        $this->cacheItemRepository = $cacheItemRepository;
        $this->logger = $logger;
        $this->enabled = $enabled;
    }

    public function updateStaleCacheByRemoteConnection(string $targetLanguage): Menus
    {
        if (!$this->enabled) {
            throw new RuntimeException('As the module is disabled, this method should not be called.');
        }

        $cacheItem = $this->cacheItemRepository->getItem($targetLanguage);
        try {
            $headerJson = $this->connection->downloadMenus($targetLanguage);

            $header = $this->serializer->deserialize($headerJson);
        } catch (RemoteConnectionException $e) {
            if ($this->logger) {
                $this->logger->warning($e->getMessage(), ['exception' => $e, 'file' => __FILE__, 'line' => __LINE__]);
            }

            $header = $cacheItem->get();
            if (!$header instanceof Menus) {
                throw new RuntimeException(
                    'Unable to fallback a connection error to stale cache as it seems to be unset. '
                    . 'If you\'re implementing, looks like you have some kind of connection problem (token?). '
                    . 'If its on live server, maybe there were a service interruption just after a cache clear and all '
                    . 'is fixed when you read this message. If not, you have a new connection problem'
                );
            }
        }

        $cacheItem->set($header);
        $this->cacheItemRepository->storeItem($cacheItem);

        return $header;
    }
}
