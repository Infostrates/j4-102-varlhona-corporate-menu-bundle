<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use DomainException;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Menus;
use Psr\Cache\InvalidArgumentException;

class ProviderByStaleCache
{
    /** @var CacheItemRepository */
    private $cacheItemProvider;

    /**
     * @param CacheItemRepository $cacheItemProvider
     */
    public function __construct(CacheItemRepository $cacheItemProvider)
    {
        $this->cacheItemProvider = $cacheItemProvider;
    }

    /**
     * @param string $targetLanguage
     * @return Menus
     * @throws MenusProviderException
     */
    public function getMenusFromStaleCache(string $targetLanguage): Menus
    {
        try {
            $cacheItem = $this->cacheItemProvider->getItem($targetLanguage);
        } catch (InvalidArgumentException $e) {
            throw new DomainException('Unable to get cache');
        }

        $value = $cacheItem->get();
        if (!$value instanceof Menus) {
            throw new MenusProviderException('No cache with a header');
        }

        return $value;
    }
}
