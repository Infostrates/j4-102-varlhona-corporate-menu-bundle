<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use DateTime;
use DomainException;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Traversable;

class CacheItemRepository
{
    private const AVAILABLE_LANGUAGES = [
        'fr',
        'en',
        'es',
        'de',
        'it',
    ];
    private const ITEM_KEY_PREFIX = 'corporate_menu_header_';
    private const LAST_UPDATE_KEY_PREFIX = 'corporate_menu_header_last_update_';

    /** @var CacheItemPoolInterface */
    private $cacheItemPool;

    public function __construct(CacheItemPoolInterface $cacheItemPool)
    {
        $this->cacheItemPool = $cacheItemPool;
    }

    public function getItem(string $targetLanguage): CacheItemInterface
    {
        try {
            return $this->cacheItemPool->getItem($this->getItemKey($targetLanguage));
        } catch (InvalidArgumentException $e) {
            throw new DomainException('Unable to get cache item', 0, $e);
        }
    }

    public function storeItem(CacheItemInterface $cacheItem): void
    {
        try {
            $lastUpdateCacheItem = $this->cacheItemPool->getItem(
                $this->getLastUpdateItemKey($this->getTargetLanguageByCacheItem($cacheItem))
            );
        } catch (InvalidArgumentException $e) {
            throw new DomainException('Unable to get last update cache item', 0, $e);
        }

        $lastUpdateCacheItem->set(new DateTime());

        $this->cacheItemPool->save($cacheItem);
        $this->cacheItemPool->save($lastUpdateCacheItem);
    }

    protected function getItemKey(string $targetLanguage): string
    {
        return self::ITEM_KEY_PREFIX . $targetLanguage;
    }

    protected function getLastUpdateItemKey(string $targetLanguage): string
    {
        return self::LAST_UPDATE_KEY_PREFIX . $targetLanguage;
    }

    /**
     * @return array<CacheItemInterface>|Traversable<CacheItemInterface>
     */
    public function getLastUpdateItems()
    {
        try {
            $keyList = array_map(function ($language) {
                return $this->getLastUpdateItemKey($language);
            }, self::AVAILABLE_LANGUAGES);

            return $this->cacheItemPool->getItems($keyList);
        } catch (InvalidArgumentException $e) {
            throw new DomainException('Unable to get cache items');
        }
    }

    public function getTargetLanguageByCacheItem(CacheItemInterface $cacheItem): string
    {
        if (
            !preg_match(
                sprintf('/%s([a-z]+)/', self::ITEM_KEY_PREFIX),
                $cacheItem->getKey(),
                $matches
            )
        ) {
            throw new \InvalidArgumentException('Not a valid cache item');
        }

        return $matches[1];
    }

    public function getTargetLanguageByLastUpdateCacheItem(CacheItemInterface $lastUpdateCacheItem): string
    {
        if (
            !preg_match(
                sprintf('/%s([a-z]+)/', self::LAST_UPDATE_KEY_PREFIX),
                $lastUpdateCacheItem->getKey(),
                $matches
            )
        ) {
            throw new \InvalidArgumentException('Not a valid last update cache item');
        }

        return $matches[1];
    }
}
