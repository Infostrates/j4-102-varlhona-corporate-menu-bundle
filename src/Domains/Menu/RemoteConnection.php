<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use DomainException;
use Exception;
use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;

class RemoteConnection
{
    /** @var string */
    private $base;

    /** @var string */
    private $baseUrl;

    /** @var string */
    private $token;

    /** @var bool */
    private $localDev;

    /** @var HttpClient|null */
    private $httpClient;

    /** @var MessageFactory|null */
    private $messageFactory;

    public function __construct(
        string $base,
        string $baseUrl,
        string $token,
        bool $localDev,
        ?HttpClient $httpClient,
        ?MessageFactory $messageFactory
    ) {
        $this->base = $base;
        $this->baseUrl = $baseUrl;
        $this->token = $token;
        $this->localDev = $localDev;
        $this->httpClient = $httpClient;
        $this->messageFactory = $messageFactory;
    }

    /**
     * @param string $targetLanguage
     * @return string
     * @throws RemoteConnectionException
     */
    public function downloadMenus(string $targetLanguage): string
    {
        $menusDownloadUrl = $this->getMenusDownloadUrl($targetLanguage);
        try {
            if (!$this->httpClient || !$this->messageFactory) {
                return $this->downloadByFileGetContent($menusDownloadUrl);
            }

            $request = $this->messageFactory->createRequest(
                'GET',
                $menusDownloadUrl
            );
            $response = $this->httpClient->sendRequest($request);

            return $response->getBody()->getContents();
        } catch (Exception | ClientExceptionInterface $e) {
            if ($this->localDev) {
                $errorMessage = 'infostrates_valrhona_corporate_menu.local_dev is enabled, and the local web valrhona '
                    . 'seems unreachable.' . "\n"
                    . 'Check that your ValrhonaRefonteSite docker is alive and healthy here : ' . $menusDownloadUrl;
                if (parse_url($menusDownloadUrl, PHP_URL_HOST) === 'localhost') {
                    $errorMessage .= "\n" . 'Also, if you intend to use a ssh tunnel, the host to set to the '
                        . 'container is : ' . $_SERVER['REMOTE_ADDR'];
                }

                throw new DomainException(
                    $errorMessage,
                    0,
                    $e
                );
            }
            throw new RemoteConnectionException('Unable to get header info', 0, $e);
        }
    }

    /**
     * @param string $targetLanguage
     * @return string
     */
    private function getMenusDownloadUrl(string $targetLanguage): string
    {
        switch ($this->base) {
            case 'asia':
            case 'eu':
                $url = sprintf('%s/%s/corporate_menu_share/menus/1/%s', $this->baseUrl, $targetLanguage, $this->token);
                break;
            case 'us':
                $url = sprintf('%s/corporate_menu_share/menus/1/%s', $this->baseUrl, $this->token);
                break;
            default:
                throw new RuntimeException('Unknown base : ' . $this->base);
        }

        return $url;
    }

    /**
     * @param string $url
     * @return string
     */
    private function downloadByFileGetContent(string $url): string
    {
        $context = stream_context_create([
            'http' => [
                'timeout' => $this->localDev ? 10 : 1,
            ]
        ]);
        $response = file_get_contents($url, false, $context);

        if (!$response) {
            throw new RuntimeException('file_get_content got an empty response');
        }
        return $response;
    }
}
