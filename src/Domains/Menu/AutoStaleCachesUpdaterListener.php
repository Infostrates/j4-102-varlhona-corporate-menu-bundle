<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Domains\Menu;

use DateTime;
use Exception;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\StaleCacheUpdater;
use RuntimeException;
use Symfony\Component\HttpKernel\Event\KernelEvent;

/**
 * This listener won't be enabled if the module is set to be disabled
 * @see \Infostrates\ValrhonaCorporateMenu\DependencyInjection\InfostratesValrhonaCorporateMenuExtension::load()
 */
class AutoStaleCachesUpdaterListener
{
    /** @var CacheItemRepository */
    private $cacheItemRepository;

    /** @var StaleCacheUpdater */
    private $staleCacheUpdater;

    /** @var int */
    private $cacheTtl;

    /** @var bool */
    private $localDev;

    public function __construct(
        CacheItemRepository $cacheItemRepository,
        StaleCacheUpdater $staleCacheUpdater,
        int $cacheTtl,
        bool $localDev
    ) {
        $this->cacheItemRepository = $cacheItemRepository;
        $this->staleCacheUpdater = $staleCacheUpdater;
        $this->cacheTtl = $cacheTtl;
        $this->localDev = $localDev;
    }

    /**
     * @param KernelEvent $event Should be TerminateEvent or PostResponseEvent, but got parent to simplifly
     *                           compatibility as we only use parent class' method
     * @return void
     */
    public function onKernelTerminate(KernelEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $request = $event->getRequest();
        $route = $request->get('_route');
        if (is_string($route) && strpos($route, '_') === 0) {
            return;
        }

        $relativeDateString = sprintf('%d seconds ago', $this->cacheTtl);
        try {
            $expirationDate = new DateTime($relativeDateString);
        } catch (Exception $e) {
            throw new RuntimeException('Unable to generate date with ' . $relativeDateString);
        }

        $lastUpdateCacheItemList = $this->cacheItemRepository->getLastUpdateItems();
        foreach ($lastUpdateCacheItemList as $lastUpdateCacheItem) {
            $lastUpdateDate = $lastUpdateCacheItem->get();
            if (!$lastUpdateDate instanceof DateTime) {
                continue;
            }


            if ($this->localDev || $lastUpdateDate < $expirationDate) {
                $this->staleCacheUpdater->updateStaleCacheByRemoteConnection(
                    $this->cacheItemRepository->getTargetLanguageByLastUpdateCacheItem($lastUpdateCacheItem)
                );
            }
        }
    }
}
