<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Actions;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer as FooterObject;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Footer extends AbstractMenuComponent
{
    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function __invoke(string $targetLanguage): Response
    {
        return $this->execute($targetLanguage, FooterObject::class);
    }
}
