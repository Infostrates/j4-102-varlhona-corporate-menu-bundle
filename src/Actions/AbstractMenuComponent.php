<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Actions;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer as FooterObject;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Menus;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\ProviderByStaleCache;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Renderer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\StaleCacheUpdater;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\MenusProviderException;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header as HeaderObject;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\MenuComponentInterface;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\WebAssets;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

abstract class AbstractMenuComponent
{
    /** @var ProviderByStaleCache */
    protected $providerByStaleCache;

    /** @var StaleCacheUpdater */
    protected $staleCacheUpdater;

    /** @var Renderer */
    protected $renderer;

    /** @var bool */
    protected $enabled;

    /** @var bool */
    protected $userMenuDisabled;

    /**
     * @param ProviderByStaleCache $providerByStaleCache
     * @param StaleCacheUpdater $staleCacheUpdater
     * @param Renderer $renderer
     * @param bool $enabled
     * @param bool $userMenuDisabled
     */
    public function __construct(
        ProviderByStaleCache $providerByStaleCache,
        StaleCacheUpdater $staleCacheUpdater,
        Renderer $renderer,
        bool $enabled,
        bool $userMenuDisabled
    ) {
        $this->providerByStaleCache = $providerByStaleCache;
        $this->staleCacheUpdater = $staleCacheUpdater;
        $this->renderer = $renderer;
        $this->enabled = $enabled;
        $this->userMenuDisabled = $userMenuDisabled;
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    abstract public function __invoke(string $targetLanguage): Response;

    /**
     * @param string       $targetLanguage
     * @param class-string $componentType
     * @param string|null  $variation
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function execute(string $targetLanguage, string $componentType, ?string $variation = null): Response
    {
        if (!$this->enabled) {
            return new Response();
        }
        //TODO http cache simple implementation with an etag corresponding to the last_update date string
        try {
            $menus = $this->providerByStaleCache->getMenusFromStaleCache($targetLanguage);
        } catch (MenusProviderException $e) {
            $menus = $this->staleCacheUpdater->updateStaleCacheByRemoteConnection($targetLanguage);
        }

        $options = [
            'user_menu_disabled' => $this->userMenuDisabled,
            'target_language' => $targetLanguage,
        ];

        return new Response($this->renderer->render(
            $this->getComponent($menus, $componentType),
            $options,
            $variation
        ));
    }

    /**
     * @param Menus $menus
     * @param class-string $componentType
     * @return MenuComponentInterface
     */
    private function getComponent(Menus $menus, string $componentType): MenuComponentInterface
    {
        switch ($componentType) {
            case HeaderObject::class:
                return $menus->getHeader();
            case FooterObject::class:
                return $menus->getFooter();
            case WebAssets::class:
                return $menus->getWebAssets();
            default:
                throw new InvalidArgumentException('Unhandled component type');
        }
    }
}
