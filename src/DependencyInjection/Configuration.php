<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder('infostrates_valrhona_corporate_menu');
        $root = $builder->getRootNode();
        $root
            ->children()
                ->scalarNode('base')->defaultValue('eu')->end()
                ->scalarNode('base_url')->defaultValue('https://www.valrhona.com')->end()
                ->integerNode('cache_ttl')->defaultValue(600)->end()
                ->scalarNode('token')->defaultValue('')->end()
                ->arrayNode('locales')
                    ->scalarPrototype()->cannotBeEmpty()->end()
                    ->defaultValue(['fr', 'en', 'es', 'de', 'it'])
                ->end()
                ->booleanNode('enabled')->defaultValue(true)->end()
                ->booleanNode('local_dev')->defaultValue(false)->end()
                ->booleanNode('user_menu_disabled')->defaultValue(false)->end()
            ->end()
        ;

        return $builder;
    }
}
