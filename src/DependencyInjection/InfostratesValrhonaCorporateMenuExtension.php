<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class InfostratesValrhonaCorporateMenuExtension extends Extension
{
    /**
     * @param array<string, mixed> $configs
     * @param ContainerBuilder $container
     * @return void
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('infostrates_valrhona_corporate_menu.actions.abstract_menu_component');
        $definition->replaceArgument(3, $config['enabled']);
        $definition->replaceArgument(4, $config['user_menu_disabled']);

        $definition = $container->getDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.stale_cache_updater'
        );
        $definition->replaceArgument(4, $config['enabled']);

        $definition = $container->getDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.warmup'
        );
        $definition->replaceArgument(4, $config['locales']);

        $definition = $container->getDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.remote_connection'
        );
        $definition->replaceArgument(0, $config['base']);
        $definition->replaceArgument(1, $config['base_url']);
        $definition->replaceArgument(2, $config['token']);
        $definition->replaceArgument(3, $config['local_dev']);

        $definition = $container->getDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener'
        );
        $definition->replaceArgument(2, $config['cache_ttl']);
        $definition->replaceArgument(3, $config['local_dev']);
    }
}
