<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\KernelEvents;

class CompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (
            !$container->hasDefinition('infostrates_valrhona_corporate_menu.domains.menu.stale_cache_updater')
            || !$container->hasDefinition(
                'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener'
            )
        ) {
            return;
        }

        $staleCacheUpdaterDefinition = $container->getDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.stale_cache_updater'
        );
        if ($container->resolveEnvPlaceholders($staleCacheUpdaterDefinition->getArgument(4), true) === false) {
            $container->removeDefinition(
                'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener'
            );

            return;
        }

        $autoStaleCachesUpdaterDefinition = $container->getDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener',
        );

        if ($container->resolveEnvPlaceholders($autoStaleCachesUpdaterDefinition->getArgument(3), true) === true) {
            $autoStaleCachesUpdaterDefinition->clearTag('kernel.event_listener');
            $autoStaleCachesUpdaterDefinition->addTag('kernel.event_listener', [
                'event' => KernelEvents::CONTROLLER,
                'method' => 'onKernelTerminate',
            ]);
        }
    }
}
