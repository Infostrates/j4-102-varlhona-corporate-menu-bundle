<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\DependencyInjection;

use Infostrates\ValrhonaCorporateMenu\DependencyInjection\CompilerPass;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class CompilerPassTest extends AbstractCompilerPassTestCase
{
    protected function registerCompilerPass(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new CompilerPass());
    }

    public function testProcess(): void
    {
        $definition = new Definition();
        $definition->setArguments([
            3 => false, //Local dev
        ]);
        $definition->addTag('kernel.event_listener', ['event' => 'kernel.terminate']);
        $this->setDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener',
            $definition
        );

        $definition = new Definition();
        $definition->setArguments([
            4 => true, //Enabled
        ]);
        $this->setDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.stale_cache_updater',
            $definition
        );

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithTag(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener',
            'kernel.event_listener',
            ['event' => 'kernel.terminate']
        );
    }

    public function testProcessWithLocalDevWithModuleDisabled(): void
    {
        $definition = new Definition();
        $definition->setArguments([
            3 => false, //Local dev
        ]);
        $definition->addTag('kernel.event_listener', ['event' => 'kernel.terminate']);
        $this->setDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener',
            $definition
        );

        $definition = new Definition();
        $definition->setArguments([
            4 => false, //Enabled
        ]);
        $this->setDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.stale_cache_updater',
            $definition
        );

        $this->compile();

        $this->assertContainerBuilderNotHasService(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener'
        );
    }

    public function testProcessWithLocalDev(): void
    {
        $definition = new Definition();
        $definition->setArguments([
            3 => true, //Local dev
        ]);
        $definition->addTag('kernel.event_listener', ['event' => 'kernel.terminate']);
        $this->setDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener',
            $definition
        );

        $definition = new Definition();
        $definition->setArguments([
            4 => true, //Enabled
        ]);
        $this->setDefinition(
            'infostrates_valrhona_corporate_menu.domains.menu.stale_cache_updater',
            $definition
        );

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithTag(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener',
            'kernel.event_listener',
            ['event' => 'kernel.controller', 'method' => 'onKernelTerminate']
        );
    }
}
