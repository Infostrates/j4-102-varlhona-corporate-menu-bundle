<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\DependencyInjection;

use DomainException;
use Infostrates\ValrhonaCorporateMenu\DependencyInjection\InfostratesValrhonaCorporateMenuExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class InfostratesValrhonaCorporateMenuExtensionTest extends AbstractExtensionTestCase
{
    /**
     * @return ExtensionInterface[]
     */
    protected function getContainerExtensions(): array
    {
        return [
            new InfostratesValrhonaCorporateMenuExtension(),
        ];
    }

    public function testLoad(): void
    {
        $this->load([
            'token' => 'test-token',
        ]);

        $this->assertContainerBuilderHasService(
            'infostrates_valrhona_corporate_menu.domains.menu.auto_stale_caches_updater_listener'
        );
    }
}
