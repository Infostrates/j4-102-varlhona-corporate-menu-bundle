<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Actions;

use Infostrates\ValrhonaCorporateMenu\Actions\Header;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\ProviderByStaleCache;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Renderer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\StaleCacheUpdater;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\MenusProviderException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class HeaderTest extends TestCase
{
    /** @var Header */
    private $testSubject;

    /** @var \Infostrates\ValrhonaCorporateMenu\Domains\Menu\StaleCacheUpdater&MockObject */
    private $staleCacheUpdaterMock;

    /** @var Renderer&MockObject */
    private $rendererMock;

    protected function setUp(): void
    {
        /** @var ProviderByStaleCache&MockObject $providerMock */
        $providerMock = $this->createMock(ProviderByStaleCache::class);

        /** @var StaleCacheUpdater&MockObject $staleCacheUpdaterMock */
        $staleCacheUpdaterMock = $this->createMock(StaleCacheUpdater::class);
        $this->staleCacheUpdaterMock = $staleCacheUpdaterMock;

        /** @var \Infostrates\ValrhonaCorporateMenu\Domains\Menu\Renderer&MockObject $rendererMock */
        $rendererMock = $this->createMock(Renderer::class);
        $this->rendererMock = $rendererMock;

        $this->testSubject = new Header(
            $providerMock,
            $staleCacheUpdaterMock,
            $rendererMock,
            true,
            false
        );
    }

    /**
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testInvoke(): void
    {
        $this->rendererMock->method('render')->willReturn('Header html content');
        $actualResponse = ($this->testSubject)('fr');

        $expectedResponse = new Response('Header html content');

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    /**
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testInvokeWithoutCache(): void
    {
        $this->staleCacheUpdaterMock
            ->method('updateStaleCacheByRemoteConnection')
            ->willThrowException(new MenusProviderException('There s no cache, even expird one'))
        ;
        $this->rendererMock->method('render')->willReturn('Just refreshed header html content');
        $actualResponse = ($this->testSubject)('fr');

        $expectedResponse = new Response('Just refreshed header html content');

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    /**
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testInvokeDisabled(): void
    {
        /** @var \Infostrates\ValrhonaCorporateMenu\Domains\Menu\ProviderByStaleCache&MockObject $providerMock */
        $providerMock = $this->createMock(ProviderByStaleCache::class);

        $this->rendererMock->method('render')->willReturn('Non used render');

        $testSubject = new Header(
            $providerMock,
            $this->staleCacheUpdaterMock,
            $this->rendererMock,
            false,
            false
        );
        $actualResponse = ($testSubject)('fr');

        $expectedResponse = new Response('');

        $this->assertEquals($expectedResponse, $actualResponse);
    }
}
