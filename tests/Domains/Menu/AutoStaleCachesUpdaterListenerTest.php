<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use DateTime;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\CacheItemRepository;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\StaleCacheUpdater;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\AutoStaleCachesUpdaterListener;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use PHPUnit\Framework\TestCase;

class AutoStaleCachesUpdaterListenerTest extends TestCase
{
    /** @var AutoStaleCachesUpdaterListener */
    private $testSubject;

    /** @var MockObject&CacheItemRepository */
    private $cacheItemRepositoryMock;

    /**  @var StaleCacheUpdater&MockObject */
    private $staleCacheUpdaterMock;

    protected function setUp(): void
    {
        /** @var CacheItemRepository&MockObject $cacheItemRepository */
        $cacheItemRepository = $this->createMock(CacheItemRepository::class);
        $this->cacheItemRepositoryMock = $cacheItemRepository;

        /** @var StaleCacheUpdater&MockObject $staleCacheUpdaterMock */
        $staleCacheUpdaterMock = $this->createMock(StaleCacheUpdater::class);
        $this->staleCacheUpdaterMock = $staleCacheUpdaterMock;

        $this->testSubject = new AutoStaleCachesUpdaterListener(
            $cacheItemRepository,
            $staleCacheUpdaterMock,
            600,
            false
        );
    }


    public function testUpdateStaleCaches(): void
    {
        $event = $this->getEvent();

        $this->cacheItemRepositoryMock->method('getLastUpdateItems')->willReturn([
            (new CacheItem())->set(new DateTime('642 seconds ago')),
            (new CacheItem())->set(new DateTime('600 seconds ago')),
            (new CacheItem())->set(new DateTime('599 seconds ago')),
            (new CacheItem())->set(null),
        ]);

        $this->staleCacheUpdaterMock->expects(TestCase::exactly(2))->method('updateStaleCacheByRemoteConnection');
        $this->testSubject->onKernelTerminate($event);
    }

    public function testUpdateStaleCachesNotMainRequest(): void
    {
        $event = $this->getEvent(false);

        $this->cacheItemRepositoryMock->expects(TestCase::never())->method('getLastUpdateItems');

        $this->testSubject->onKernelTerminate($event);
    }

    public function testUpdateStaleCachesWithLocalDev(): void
    {
        $testSubject = new AutoStaleCachesUpdaterListener(
            $this->cacheItemRepositoryMock,
            $this->staleCacheUpdaterMock,
            600,
            true
        );
        $event = $this->getEvent();

        $this->cacheItemRepositoryMock->method('getLastUpdateItems')->willReturn([
            (new CacheItem())->set(new DateTime('642 seconds ago')),
            (new CacheItem())->set(new DateTime('600 seconds ago')),
            (new CacheItem())->set(new DateTime('599 seconds ago')),
            (new CacheItem())->set(null),
        ]);

        $this->staleCacheUpdaterMock->expects(TestCase::exactly(3))->method('updateStaleCacheByRemoteConnection');
        $testSubject->onKernelTerminate($event);
    }

    private function getEvent(bool $isMainRequest = true): KernelEvent
    {
        /** @var KernelEvent&MockObject $eventMock */
        $eventMock = $this->createMock(KernelEvent::class);
        $eventMock->method('isMainRequest')->willReturn($isMainRequest);
        $eventMock->method('getRequest')->willReturn(new Request());
        return $eventMock;
    }
}
