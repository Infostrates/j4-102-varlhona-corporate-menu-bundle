<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use DomainException;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Renderer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class RendererTest extends KernelTestCase
{
    /** @var Renderer */
    private $testSubject;

    protected function setUp(): void
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();
        if (!$container) {
            throw new DomainException('No container available');
        }
        $testSubject = $container->get(
            'infostrates_valrhona_corporate_menu.domains.menu.renderer'
        );
        if (!$testSubject instanceof Renderer) {
            throw new DomainException('Unexpected service returned');
        }
        $this->testSubject = $testSubject;
    }

    /**
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testRenderHeader(): void
    {
        $result = $this->testSubject->render(MenusTestObjects::getTestHeader(), [
            'target_language' => 'fr',
        ]);
        $this->assertNotEmpty($result);
    }

    /**
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testRenderWebAssets(): void
    {
        $result = $this->testSubject->render(MenusTestObjects::getTestWebAssets(), [
            'target_language' => 'fr',
        ], 'js');
        $this->assertNotEmpty($result);

        $result = $this->testSubject->render(MenusTestObjects::getTestWebAssets(), [
            'target_language' => 'fr',
        ], 'css');
        $this->assertNotEmpty($result);
    }

    /**
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testRenderFooter(): void
    {
        $result = $this->testSubject->render(MenusTestObjects::getTestFooter(), [
            'target_language' => 'fr',
        ]);
        $this->assertNotEmpty($result);
    }
}
