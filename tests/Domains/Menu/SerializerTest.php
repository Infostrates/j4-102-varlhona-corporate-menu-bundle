<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use DomainException;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Serializer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Button;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\EventBanner;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\MenuItem;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class SerializerTest extends KernelTestCase
{
    /** @var Serializer */
    private $testSubject;

    /**
     * @return iterable<array<string>>
     */
    public function jsonProvider(): iterable
    {
        $projectDir = $this->getProjectDir();
        $jsonFolder = $projectDir . '/tests/Resources/menus';
        $files = (new Finder())->files()->name('*.json')->in($jsonFolder);

        foreach ($files as $file) {
            yield [(string)file_get_contents($file)];
        }
    }

    protected function setUp(): void
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();
        if (!$container) {
            throw new DomainException('No container available');
        }
        $testSubject = $container->get(
            'infostrates_valrhona_corporate_menu.domains.menu.serializer'
        );
        if (!$testSubject instanceof Serializer) {
            throw new DomainException('Unexpected service returned');
        }
        $this->testSubject = $testSubject;
    }

    public function testSerializer(): void
    {
        $actualJson = $this->testSubject->serialize(MenusTestObjects::getTestMenus());
        $this->assertJsonStringEqualsJsonFile($this->getProjectDir() . '/tests/Resources/menus/test.json', $actualJson);
    }

    /**
     * @dataProvider jsonProvider
     * @param string $json
     * @return void
     */
    public function testDeserialize(string $json): void
    {
        $result = $this->testSubject->deserialize($json);
        $this->assertContainsOnlyInstancesOf(EventBanner::class, $result->getHeader()->getEventBannerList());
        $this->assertContainsOnlyInstancesOf(Button::class, $result->getHeader()->getButtonList());
        $this->assertContainsOnlyInstancesOf(MenuItem::class, $result->getHeader()->getMainMenu());
    }

    /**
     * @return string
     */
    protected function getProjectDir(): string
    {
        return dirname(__DIR__, 3);
    }
}
