<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use DomainException;
use Exception;
use GuzzleHttp\Psr7\Response;
use Http\Client\Exception\NetworkException;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\RemoteConnection;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\RemoteConnectionException;
use PHPStan\BetterReflection\Reflection\ReflectionAttribute;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class RemoteConnectionTest extends KernelTestCase
{
    /** @var RemoteConnection */
    private $testSubject;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var Container $container */
        $container = self::$kernel->getContainer();
        /** @var RemoteConnection $testSubject */
        $testSubject = $container->get(
            'infostrates_valrhona_corporate_menu.domains.menu.remote_connection'
        );
        $this->testSubject = $testSubject;
    }

    public function testDownloadMenus(): void
    {
        /** @var Container $container */
        $container = self::$kernel->getContainer();
        /** @var Client $mockClient */
        $mockClient = $container->get('httplug.client.mock');
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $expectedResponseString = 'Good response';
        $expectedResponse = new Response(200, [], $expectedResponseString);
        $mockClient->on(
            new RequestMatcher(
                '/fr/corporate_menu_share/menus/1/test-token',
                'www.valrhona.com',
                'get',
                'https'
            ),
            $expectedResponse
        );

        $this->assertSame(
            $expectedResponseString,
            $this->testSubject->downloadMenus('fr')
        );
    }

    public function testDownloadMenusFail(): void
    {
        /** @var Container $container */
        $container = self::$kernel->getContainer();
        /** @var Client $mockClient */
        $mockClient = $container->get('httplug.client.mock');
        /** @var NetworkException&MockObject $mockNetworkException */
        $mockNetworkException = $this->createMock(NetworkException::class);
        $mockClient->addException($mockNetworkException);

        $this->expectException(RemoteConnectionException::class);
        $this->testSubject->downloadMenus('fr');
    }

    public function testDownloadMenusFailOnDev(): void
    {
        /** @var Container $container */
        $container = self::$kernel->getContainer();
        /** @var Client $mockClient */
        $mockClient = $container->get('httplug.client.mock');
        /** @var NetworkException&MockObject $mockNetworkException */
        $mockNetworkException = $this->createMock(NetworkException::class);
        $mockClient->addException($mockNetworkException);

        $attribute = new ReflectionProperty(RemoteConnection::class, 'localDev');
        $attribute->setAccessible(true);
        $attribute->setValue($this->testSubject, true);

        $this->expectException(DomainException::class);
        $this->testSubject->downloadMenus('fr');
    }
}
