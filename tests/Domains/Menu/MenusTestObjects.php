<?php

declare(strict_types=1);

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Button;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\DropdownItem;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Image;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\Link;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\MainLogo;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Common\MenuItem;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Footer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\UserMenu;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\AdditionalAssets;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\AdditionalWordings;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\EventBanner;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\Push;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Header\Search;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\Menus;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Model\WebAssets;

final class MenusTestObjects
{
    public static function getTestMenus(): Menus
    {
        return new Menus(
            self::getTestWebAssets(),
            self::getTestHeader(),
            self::getTestFooter()
        );
    }

    public static function getTestHeader(): Header
    {
        return new Header(
            [
                new EventBanner('Test', 'https://www.infostrates.fr'),
                new EventBanner('Test2', 'https://infostrates.fr'),
            ],
            new MainLogo(
                new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                'https://www.infostrates.fr'
            ),
            new Search(
                new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                'Test tooltip',
                'https://www.infostrates.fr/search',
                'term',
                'Recherchez u nprodui une formation une recette'
            ),
            [
                new Button(
                    new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                    'Test tooltip',
                    new Link('https://www.infostrates.fr'),
                ),
                new Button(
                    new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                    'Test tooltip',
                    new Link('https://infostrates.fr/search', Link::TARGET_BLANK),
                )
            ],
            [
                new MenuItem(
                    '100 years',
                    new Link('/test-100-years'),
                    null,
                    [],
                    null,
                    'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'
                ),
                new MenuItem('Test link', new Link('https://www.infostrates.fr')),
                new MenuItem('Test link2', new Link('https://infostrates.fr')),
                new MenuItem(
                    'Test parent',
                    null,
                    'test',
                    [
                        new MenuItem('Test children', new Link('/test-children'))
                    ],
                    new Push(
                        new Image(
                            'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'
                        ),
                        'Test Push',
                        new Link('https://infostrates.fr/', Link::TARGET_BLANK)
                    )
                )
            ],
            new AdditionalAssets(
                'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg',
                'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg',
                'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg',
                'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'
            ),
            new AdditionalWordings('Voir'),
            new UserMenu(
                'Compte client',
                '/saml/login',
                '/assets/build/images/user.svg',
                'Se déconnecter',
                '/saml/logout',
                '/assets/build/images/close_modal.svg',
                [
                    new DropdownItem(
                        'Mes services',
                        '/fr/mes-services',
                        '_self'
                    ),
                    new DropdownItem(
                        'Mes transactions',
                        '/fr/transactions',
                        '_self'
                    )
                ]
            ),
            null
        );
    }

    public static function getTestWebAssets(): WebAssets
    {
        return new WebAssets(
            ["http://localhost:8018/assets/build/css/header.css"],
            ["http://localhost:8018/assets/build/js/header.js"],
        );
    }

    public static function getTestFooter(): Footer
    {
        return new Footer(
            new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
            [
                new Footer\Zone('Test1', 'test1', [
                    new Footer\CountryAccess(
                        'Test1-1',
                        't1-1',
                        [new Footer\LanguageAccess('Français', 'fr', 'fr', 'https://infostrates.fr')]
                    ),
                    new Footer\CountryAccess(
                        'Test1-2',
                        't1-2',
                        [new Footer\LanguageAccess('Français', 'fr', 'fr', 'https://infostrates.fr')]
                    )
                ]),
            ],
            new Footer\CountryAccess(
                'Test1-1',
                't1-1',
                [new Footer\LanguageAccess('Français', 'fr', 'fr', 'https://infostrates.fr')]
            ),
            [
                new Footer\LanguageAccess('Français', 'fr', 'fr', 'https://infostrates.fr'),
            ],
            new Footer\LanguageAccess('Français', 'fr', 'fr', 'https://infostrates.fr'),
            new Footer\MenuItemsContainer(
                'Container 1',
                [
                    new MenuItem('Link 1', new Link('https://infostrates.fr')),
                    new MenuItem('Link 2', new Link('https://www.infostrates.fr')),
                ]
            ),
            new Footer\MenuItemsContainer(
                'Container 2',
                [
                    new MenuItem('Link 1', new Link('https://infostrates.fr')),
                    new MenuItem('Link 2', new Link('https://www.infostrates.fr')),
                ]
            ),
            new Footer\ButtonsContainer(
                'Button container',
                [
                    new Button(
                        new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                        'Test tooltip',
                        new Link('https://www.infostrates.fr'),
                    ),
                    new Button(
                        new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                        'Test tooltip',
                        new Link('https://infostrates.fr/search', Link::TARGET_BLANK),
                    )
                ]
            ),
            new Footer\BCorpIcon(
                new Image('https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg'),
                'https://infostrates.fr'
            ),
            new Footer\MenuItemsContainer(
                'Container 3',
                [
                    new MenuItem('Link 1', new Link('https://infostrates.fr')),
                    new MenuItem('Link 2', new Link('https://www.infostrates.fr')),
                ]
            ),
            new Footer\AdditionalAssets(
                'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg',
                'https://infostrates.fr/assets/build/images/infostrates.085e3b29.svg',
            ),
            new Footer\AdditionalWordings(
                'Label 1',
                'Label 2',
                'Label 3',
                'Label 4',
            )
        );
    }
}
