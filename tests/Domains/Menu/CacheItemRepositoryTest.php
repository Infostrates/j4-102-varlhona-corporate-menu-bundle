<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use Closure;
use DateTime;
use DomainException;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\CacheItemRepository;
use InvalidArgumentException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\CacheItem;

class CacheItemRepositoryTest extends TestCase
{
    /** @var CacheItemRepository */
    private $testSubject;

    /** @var CacheItemPoolInterface&MockObject */
    private $cachePoolMock;

    protected function setUp(): void
    {
        /** @var CacheItemPoolInterface&MockObject $cachePoolMock */
        $cachePoolMock = $this->createMock(CacheItemPoolInterface::class);
        $this->cachePoolMock = $cachePoolMock;

        $this->testSubject = new CacheItemRepository($this->cachePoolMock);
    }

    public function testGetItem(): void
    {
        $cacheItem = new CacheItem();
        $this->cachePoolMock->method('getItem')
            ->with('corporate_menu_header_fr')
            ->willReturn($cacheItem)
        ;

        $result = $this->testSubject->getItem('fr');
        $this->assertSame($cacheItem, $result);
    }

    public function testStoreItem(): void
    {
        $cacheItem = $this->createCacheItemWithKey('corporate_menu_header_fr');

        $expectedLastUpdateKey = 'corporate_menu_header_last_update_fr';
        $lastUpdateCacheItem = $this->createCacheItemWithKey($expectedLastUpdateKey);
        $this->cachePoolMock->expects(TestCase::once())->method('getItem')
            ->with($expectedLastUpdateKey)
            ->willReturn($lastUpdateCacheItem)
        ;
        $this->cachePoolMock->expects(TestCase::exactly(2))->method('save');

        $this->testSubject->storeItem($cacheItem);
        $lastUpdateDate = $lastUpdateCacheItem->get();
        if (!$lastUpdateDate instanceof DateTime) {
            throw new DomainException('Unexpected last update date type');
        }
        $this->assertEquals(0, (new DateTime())->diff($lastUpdateDate)->s);
    }

    public function testGetLastUpdateItems(): void
    {
        $expectedResult = [
            (new CacheItem())->set(new DateTime('642 seconds ago')),
            (new CacheItem())->set(new DateTime('600 seconds ago')),
            (new CacheItem())->set(new DateTime('599 seconds ago')),
            (new CacheItem())->set(null),
            (new CacheItem())->set(null),
        ];
        $this->cachePoolMock->method('getItems')
            ->with([
                'corporate_menu_header_last_update_fr',
                'corporate_menu_header_last_update_en',
                'corporate_menu_header_last_update_es',
                'corporate_menu_header_last_update_de',
                'corporate_menu_header_last_update_it',
            ])
            ->willReturn($expectedResult)
        ;

        $actualResult = $this->testSubject->getLastUpdateItems();
        $this->assertSame($expectedResult, $actualResult);
    }

    public function testGetTargetLanguageByLastUpdateCacheItem(): void
    {
        $this->assertSame(
            'fr',
            $this->testSubject->getTargetLanguageByLastUpdateCacheItem(
                $this->createCacheItemWithKey('corporate_menu_header_last_update_fr')
            )
        );

        $this->assertSame(
            'kr',
            $this->testSubject->getTargetLanguageByLastUpdateCacheItem(
                $this->createCacheItemWithKey('corporate_menu_header_last_update_kr')
            )
        );
    }

    public function testGetTargetLanguageByLastUpdateCacheItemFail(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->testSubject->getTargetLanguageByLastUpdateCacheItem(
            $this->createCacheItemWithKey('not_a_valid_last_update_key_fr')
        );
    }

    private function createCacheItemWithKey(string $key): CacheItem
    {
        static $createCacheItemClosure;
        if ($createCacheItemClosure === null) {
            $createCacheItemClosure = Closure::bind(
                static function ($key) {
                    $item = new CacheItem();
                    $item->key = $key;

                    return $item;
                },
                null,
                CacheItem::class
            );
        }

        return ($createCacheItemClosure)($key);
    }
}
