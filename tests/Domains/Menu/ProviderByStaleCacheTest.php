<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\CacheItemRepository;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\ProviderByStaleCache;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\MenusProviderException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\CacheItem;

class ProviderByStaleCacheTest extends TestCase
{
    /** @var ProviderByStaleCache */
    private $testSubject;

    /** @var CacheItemRepository&MockObject */
    private $cacheItemProviderMock;

    protected function setUp(): void
    {
        /** @var CacheItemRepository&MockObject $cacheItemProviderMock */
        $cacheItemProviderMock = $this->createMock(CacheItemRepository::class);
        $this->cacheItemProviderMock = $cacheItemProviderMock;

        $this->testSubject = new ProviderByStaleCache(
            $this->cacheItemProviderMock
        );
    }


    /**
     * @return void
     * @throws MenusProviderException
     */
    public function testGetMenusFromStaleCacheWithCacheIsHit(): void
    {
        $testMenus = MenusTestObjects::getTestMenus();

        $cacheItem = new CacheItem();
        $cacheItem->set($testMenus);
        $cacheItem->expiresAfter(600);
        $this->cacheItemProviderMock->method('getItem')->willReturn($cacheItem);

        $result = $this->testSubject->getMenusFromStaleCache('fr');
        $this->assertSame($testMenus, $result);
    }

    /**
     * @return void
     * @throws MenusProviderException
     */
    public function testGetMenusFromStaleCacheWithCacheIsNotHit(): void
    {
        $testMenus = MenusTestObjects::getTestMenus();

        $cacheItem = new CacheItem();
        $cacheItem->set($testMenus);
        $cacheItem->expiresAfter(-600);
        $this->cacheItemProviderMock->method('getItem')->willReturn($cacheItem);

        $result = $this->testSubject->getMenusFromStaleCache('fr');
        $this->assertSame($testMenus, $result);
    }

    /**
     * @return void
     * @throws MenusProviderException
     */
    public function testGetMenusFromStaleCacheWithCacheEmpty(): void
    {
        $cacheItem = new CacheItem();
        $this->cacheItemProviderMock->method('getItem')->willReturn($cacheItem);

        $this->expectException(MenusProviderException::class);
        $this->testSubject->getMenusFromStaleCache('fr');
    }
}
