<?php

namespace Infostrates\ValrhonaCorporateMenu\Tests\Domains\Menu;

use Infostrates\ValrhonaCorporateMenu\Domains\Menu\CacheItemRepository;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\RemoteConnection;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\RemoteConnectionException;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\Serializer;
use Infostrates\ValrhonaCorporateMenu\Domains\Menu\StaleCacheUpdater;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Cache\CacheItem;

class StaleCacheUpdaterTest extends TestCase
{
    /** @var StaleCacheUpdater */
    private $testSubject;

    /** @var RemoteConnection&MockObject */
    private $connectionMock;

    /** @var Serializer&MockObject */
    private $serializerMock;

    /** @var CacheItemRepository&MockObject */
    private $cacheItemProvider;

    /** @var LoggerInterface&MockObject */
    private $loggerMock;

    protected function setUp(): void
    {
        /** @var RemoteConnection&MockObject $connectionMock */
        $connectionMock = $this->createMock(RemoteConnection::class);
        $this->connectionMock = $connectionMock;

        /** @var \Infostrates\ValrhonaCorporateMenu\Domains\Menu\Serializer&MockObject $serializerMock */
        $serializerMock = $this->createMock(Serializer::class);
        $this->serializerMock = $serializerMock;

        /** @var CacheItemRepository&MockObject $cacheItemRepositoryMock */
        $cacheItemRepositoryMock = $this->createMock(CacheItemRepository::class);
        $this->cacheItemProvider = $cacheItemRepositoryMock;

        /** @var LoggerInterface&MockObject $loggerMock */
        $loggerMock = $this->createMock(LoggerInterface::class);
        $this->loggerMock = $loggerMock;

        $this->testSubject = new StaleCacheUpdater(
            $this->connectionMock,
            $this->serializerMock,
            $this->cacheItemProvider,
            $this->loggerMock,
            true
        );
    }

    public function testUpdateStaleCacheByRemoteConnection(): void
    {
        $menus = MenusTestObjects::getTestMenus();

        $this->connectionMock->method('downloadMenus')->willReturn('SomeJsonString');
        $this->serializerMock->method('deserialize')->willReturn($menus);
        $cacheItem = new CacheItem();
        $this->cacheItemProvider->method('getItem')->willReturn($cacheItem);
        $this->cacheItemProvider->method('storeItem')->with($cacheItem);

        $result = $this->testSubject->updateStaleCacheByRemoteConnection('fr');
        $this->assertSame($menus, $result);
    }

    public function testUpdateStaleCacheByRemoteConnectionDisabled(): void
    {
        $testSubject = new StaleCacheUpdater(
            $this->connectionMock,
            $this->serializerMock,
            $this->cacheItemProvider,
            null,
            false
        );

        $this->expectExceptionObject(
            new RuntimeException('As the module is disabled, this method should not be called.')
        );
        $testSubject->updateStaleCacheByRemoteConnection('fr');
    }

    public function testUpdateStaleCacheByRemoteConnectionConnectionErrorWithExistingCache(): void
    {
        $menus = MenusTestObjects::getTestMenus();

        $this->connectionMock->method('downloadMenus')->willThrowException(
            new RemoteConnectionException('Some connection problem occured')
        );
        $cacheItem = new CacheItem();
        $cacheItem->set($menus);
        $this->cacheItemProvider->method('getItem')->willReturn($cacheItem);
        $this->cacheItemProvider->method('storeItem')->with($cacheItem);
        $this->loggerMock->expects(TestCase::once())->method('warning');

        $result = $this->testSubject->updateStaleCacheByRemoteConnection('fr');
        $this->assertSame($menus, $result);
    }

    public function testUpdateStaleCacheByRemoteConnectionConnectionErrorWithoutExistingCache(): void
    {
        $this->connectionMock->method('downloadMenus')->willThrowException(
            new RemoteConnectionException('Some connection problem occured')
        );
        $cacheItem = new CacheItem();
        $this->cacheItemProvider->method('getItem')->willReturn($cacheItem);

        $this->expectException(RuntimeException::class);
        $this->testSubject->updateStaleCacheByRemoteConnection('fr');
    }
}
