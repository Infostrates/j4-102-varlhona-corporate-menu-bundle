# !!! ATTENTION: Versions >= 2.2.0 is only for Symfony 6.4 and higher. !!!

Infostrates Valrhona corporate menu bundle
==========================================
Bundle to use the corporate menu, fetched from the corporate site, in another site.

Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer config repositories.valrhona_corporate_menu vcs https://bitbucket.org/Infostrates/j4-102-varlhona-corporate-menu-bundle.git
$ composer require infostrates/valrhona-corporate-menu
```

### Step 2: Only for applications that don't use Symfony Flex, enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Infostrates\ValrhonaCorporateMenu\InfostratesValrhonaCorporateMenuBundle::class => ['all' => true],
];
```
### Step 3: Add consumption token / configuration

Add the following block in the ```config/packages/infostrates_valrhona_corporate_menu.yaml``` (as a new file) :
```yaml
infostrates_valrhona_corporate_menu:
    token: <consumption token registered on the Valrhona corporate site>
    # Base possibility values are : eu, us, asia
    base: eu
    # All the following lines are optinal, with their default value, if you need to tweak it a bit:
    # If you need to set the staging corporate site (or local dev, see below for additional advice for this)
    base_url: https://www.valrhona.com
    # All the locales you want to cache during the cache warmup (ISO 639-1)
    locales: ['fr','en','es','de','it']
    # Cache life time in second between 2 remote call
    cache_ttl: 600
    # Enable remote call and menu rendering, useful for debug or for the corporate site side
    enabled: true
    # Disable the stale cache and add warning message in case of connection error. Useful for local dev on "remote project", with base_url,
    # BUT WILL DEGRADE PERFORMANCE
    local_dev: false
    # Enable/disable the user menu (if SSO is not used), false by default
    user_menu_disabled: false
```
Please note that the consumption token isn't really sensitive, so you can feel free to store it on the repository.

... but, you might want to set it by a variable on the .env file to handle multiple settings based on environment.

You might want to disable this bundle for your tests and CI, to save some request to real server. The menu render should be simply blank (no menu).
Add the following block in the ```config/packages/test/infostrates_valrhona_corporate_menu.yaml``` (as a new file) :
```yaml
infostrates_valrhona_corporate_menu:
    enabled: false
```

### Step 4: Add header in your layout template

At the appropriate place in your layout twig put the following lines :

Css calls:
```twig
{{ render_esi(controller('Infostrates\\ValrhonaCorporateMenu\\Actions\\WebAssetsCss', {'targetLanguage': 'en'})) }}
```

Header:
```twig
{{ render_esi(controller('Infostrates\\ValrhonaCorporateMenu\\Actions\\Header', {'targetLanguage': 'en'})) }}
```

Footer:
```twig
{{ render_esi(controller('Infostrates\\ValrhonaCorporateMenu\\Actions\\Footer', {'targetLanguage': 'en'})) }}
```

Js calls:
```twig
{{ render_esi(controller('Infostrates\\ValrhonaCorporateMenu\\Actions\\WebAssetsJs', {'targetLanguage': 'en'})) }}
```

For targetLanguage, you can use one of : 'fr', 'en', 'es', 'de' or 'it'
